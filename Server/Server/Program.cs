﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Diagnostics;

namespace Server
{
    class Program
    {
        static readonly Dictionary<int, TcpClient> list_clients = new Dictionary<int, TcpClient>();
        public static playerData.UserData[] savedUser = new playerData.UserData[]
            {
                new playerData.UserData("joko", "widodo"),
                new playerData.UserData("admin", "admin"),
                new playerData.UserData("user", "user")
            };
        public static int serverStamp = 0;
        private static bool isRunning = false;
        static void Main(string[] args)
        {
            Console.Title = "Game Server";
            isRunning = true;

            Thread mainThread = new Thread(new ThreadStart(MainThread));
            mainThread.Start();

            Server.Start(30, 8080);
        }
        void Play()
        {
            int count = 1;
            TcpListener ServerSocket = new TcpListener(IPAddress.Any, 8080);
            ServerSocket.Start();
            while (true)
            {
                Console.WriteLine("Menunggu pemain terhubung...");
                TcpClient client = ServerSocket.AcceptTcpClient();
                list_clients.Add(count, client);
                Console.WriteLine("Seseorang terhubung!");

                Thread t;
                t = new Thread(handle_clients);
                t.Start(count);
                count++;
            }
        }
        private static void MainThread()
        {
            Console.WriteLine($"Main thread started. Running at {Constants.TICKS_PER_SEC} ticks per second.");
            DateTime _nextLoop = DateTime.Now;

            while (isRunning)
            {
                while (_nextLoop < DateTime.Now)
                {
                    // If the time for the next loop is in the past, aka it's time to execute another tick
                    GameLogic.Update(); // Execute game logic

                    _nextLoop = _nextLoop.AddMilliseconds(Constants.MS_PER_TICK); // Calculate at what point in time the next tick should be executed

                    if (_nextLoop > DateTime.Now)
                    {
                        // If the execution time for the next tick is in the future, aka the server is NOT running behind
                        Thread.Sleep(_nextLoop - DateTime.Now); // Let the thread sleep until it's needed again.
                    }
                }
            }
        }
        public static void handle_clients(object o)
        {
            int id = (int)o;
            TcpClient client;

            client = list_clients[id];
            try
            {
                NetworkStream ns = client.GetStream();
                StreamReader reader = new StreamReader(client.GetStream());
                StreamWriter writer = new StreamWriter(client.GetStream());
                string pesan = String.Empty;
                string username = String.Empty;
                string reportLogin = "fail";
                byte[] sendBuffer = null;
                byte[] receivedBytes;
                //var timer = new Stopwatch();

                //cek username password user dan feedback
                while ((receivedBytes = GetResponse(ns)).Length > 0 && reportLogin == "fail")
                {
                    //menerima byte class user data dari client dan deserialize
                    playerData.UserData user = ByteSerialization.Deserialize<playerData.UserData>(receivedBytes);
                    Console.WriteLine("Username : " + user.Username);
                    Console.WriteLine("Password : " + user.Passsword);
                    for (int i = 0; i < savedUser.Length; i++)
                    {
                        if (user.Username == savedUser[i].Username && user.Passsword == savedUser[i].Passsword)
                        {
                            reportLogin = "success";
                            username = user.Username;
                            break;
                        }
                        else
                        {
                            reportLogin = "fail";
                        }
                    }
                    username = user.Username;
                    writer.WriteLine(reportLogin);
                    writer.Flush();
                    Console.WriteLine("login attempt status : " + reportLogin);
                }

                while ((receivedBytes = GetResponse(ns)).Length > 0)
                {
                    pesan = Encoding.ASCII.GetString(receivedBytes);
                    int receivedStamp = int.Parse(pesan.Substring(1));
                    if (serverStamp <= receivedStamp) { serverStamp = receivedStamp + 1; }
                    if (pesan.Substring(0, 1) == "C")
                    {
                        Random rnd = new Random();
                        string[] colorNames = { "red", "green", "blue" };
                        int randIndex = rnd.Next(colorNames.Length);
                        playerData.UserAtribut userAtribut = new playerData.UserAtribut(colorNames[randIndex], 0, username, serverStamp);
                        sendBuffer = ByteSerialization.Serialize(userAtribut);
                        broadcast(sendBuffer);
                        Console.WriteLine("Mengirim warna " + colorNames[randIndex] + " => " + username + ", Timestamp : " + serverStamp);
                    }
                    else if (pesan.Substring(0, 1) == "M")
                    {
                        Random rnd = new Random();
                        int randIndex = rnd.Next(1, 6);
                        playerData.UserAtribut userAtribut = new playerData.UserAtribut(String.Empty, randIndex, username, serverStamp);
                        sendBuffer = ByteSerialization.Serialize(userAtribut);
                        broadcast(sendBuffer);
                        Console.Write("Mengirim speed " + randIndex + " => " + username + ", Timestamp : " + serverStamp);
                    }
                }
                list_clients.Remove(id);
                client.Client.Shutdown(SocketShutdown.Both);
                reader.Close();
                writer.Close();
                client.Close();
                Console.WriteLine("Memutuskan koneksi.");
            }
            catch (IOException)
            {
                Console.WriteLine("Menutup thread.");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
        }
        public static void broadcast(string data)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(data + Environment.NewLine);
            {
                foreach (TcpClient c in list_clients.Values)
                {
                    NetworkStream stream = c.GetStream();
                    stream.Write(buffer, 0, buffer.Length);
                }
            }
        }
        public static void broadcast(byte[] buffer)
        {
            {
                foreach (TcpClient c in list_clients.Values)
                {
                    NetworkStream stream = c.GetStream();
                    stream.Write(buffer, 0, buffer.Length);
                }
            }
        }
        public static byte[] GetResponse(NetworkStream stream)
        {
            byte[] data = new byte[1024];
            using (MemoryStream memoryStream = new MemoryStream())
            {
                do
                {
                    var readCount = stream.Read(data, 0, data.Length);
                    memoryStream.Write(data, 0, readCount);
                } while (stream.DataAvailable);

                return memoryStream.ToArray();
            }
        }
    }
    class Constants
    {
        public const int TICKS_PER_SEC = 30;
        public const float MS_PER_TICK = 1000f / TICKS_PER_SEC;
    }
}