﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace playerData
{
    [Serializable]
    public class UserAtribut
    {
        public string Color;
        public int Speed;
        public string Username;
        public int TimeStamp = 0;
        public UserAtribut(string color, int speed, string username, int timestamp)
        {
            Color = color;
            Speed = speed;
            Username = username;
            TimeStamp = timestamp;
        }
    }
}
