﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class DieZone : MonoBehaviour
{
    bool stopwatchActive = false;
    float currentTime;
    private Transform ZonaMati;

    private Vector3 zoneSize;
    private Vector3 zonePosition;

    private Vector3 targetZoneSize;
    private Vector3 targetZonePosition;

    private float zoneSpeed;
    //public Text currentTimeText;

    // Start is called before the first frame update
    void Start()
    {
        zoneSpeed = 0.28f;

        ZonaMati = transform.Find("zone");
        setZoneSize(new Vector3(-0.6499689f, -1f), new Vector3(6, 6));

        currentTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (stopwatchActive == true)
        {
            currentTime = currentTime + Time.deltaTime;
        }
        TimeSpan time = TimeSpan.FromSeconds(currentTime);
        if (time.TotalSeconds >= 30 && time.TotalSeconds < 31)
        {
            ServerSend.ZoneStart((int)currentTime, zonePosition, zoneSize);
        }
        if (time.TotalSeconds >= 40 && time.TotalSeconds < 70) //zona 1
        {
            scaleDownZone1();
        }
        if (time.TotalSeconds >= 110 && time.TotalSeconds < 111)
        {
            ServerSend.ZoneStart((int)currentTime, zonePosition, zoneSize);
        }
        if (time.TotalSeconds >= 120) //zona 1
        {
            scaleDownZone2();
        }
    }

    private void setZoneSize(Vector3 position,Vector3 size)
    {
        zonePosition = position;
        zoneSize = size;

        transform.position = position;

        ZonaMati.localScale = size;
    }
    

    public void scaleDownZone1()
    {
       
        targetZoneSize = new Vector3(2f, 2f);
        targetZonePosition = new Vector3(-0.5f, 1.1f);

        Vector3 sizeChange = (targetZoneSize - zoneSize).normalized;
        Vector3 newZoneSize = zoneSize + sizeChange * Time.deltaTime * zoneSpeed;

        Vector3 zoneMoveDir = (targetZonePosition - zonePosition).normalized;
        Vector3 newZonePosition = zonePosition + zoneMoveDir * Time.deltaTime * zoneSpeed;
        

        setZoneSize(newZonePosition, newZoneSize);
        ServerSend.ZoneStart((int)currentTime, zonePosition, zoneSize);
    }
    public void scaleDownZone2()
    {
    
        targetZoneSize = new Vector3(0.5f, 0.5f);
        targetZonePosition = new Vector3(0f, 2.5f);

        Vector3 sizeChange = (targetZoneSize - zoneSize).normalized;
        Vector3 newZoneSize = zoneSize + sizeChange * Time.deltaTime * zoneSpeed;

        Vector3 zoneMoveDir = (targetZonePosition - zonePosition).normalized;
        Vector3 newZonePosition = zonePosition + zoneMoveDir * Time.deltaTime * zoneSpeed;
        

        setZoneSize(newZonePosition, newZoneSize);
        ServerSend.ZoneStart((int)currentTime, zonePosition, zoneSize);
    }

    public void StartStopwatch()
    {
        stopwatchActive = true;
    }

    public void StopStopwatch()
    {
        stopwatchActive = false;
    }

    public void ResetStopwatch()
    {
        stopwatchActive = false;
        currentTime = 0;
        setZoneSize(new Vector3(-0.6499689f, -1f), new Vector3(6, 6));
    }

}
