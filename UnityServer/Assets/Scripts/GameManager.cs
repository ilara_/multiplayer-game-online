﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public static bool gameStart;
    public GameObject readyCount;
    public GameObject stopwatchZone;
    public static int playerAlive;
    public static List<int> playerRankSort = new List<int>();
    public void DeadCount(int id)
    {
        playerAlive--;
        playerRankSort.Add(id);
        Debug.Log("current player alive : " + playerAlive);
        if (playerAlive == 1)
        {
            playerRankSort.Add(getMissingNo(playerRankSort, playerRankSort.Count)); //getting last man standing id
            ServerSend.GameOver(playerRankSort);
            ResetGame();
        }
    }
    static int getMissingNo(List<int> a, int n)
    {
        int total = (n + 1) * (n + 2) / 2;

        for (int i = 0; i < n; i++)
            total -= a[i];

        return total;
    }
    public void ResetGame()
    {
        Debug.Log("server reset gamenya");
        gameStart = false;
        playerAlive = 0;
        playerRankSort.Clear();

        readyCount.GetComponent<ReadyCountdown>().Reset();
        stopwatchZone.GetComponent<DieZone>().ResetStopwatch();
    }
}
