﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class knockback : MonoBehaviour
{
    public float thrust;
    public float knocktime;
    Player player;
    void Start()
    {
        try
        {
            player = GetComponent<Player>();
        }
        catch
        {
            Debug.Log("cant get player class");
        }
    }
    // Start is called before the first frame update
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (this.enabled && collision.gameObject.CompareTag("Player"))
        {
            //Rigidbody2D rbP2 = collision.gameObject.GetComponent<Rigidbody2D>();
            Player P2 = collision.gameObject.GetComponent<Player>();

            if (P2 != null)
            {
                //rbP2.isKinematic = false;
                //Vector2 difference = rbP2.transform.position - transform.position;
                //difference = -difference.normalized * thrust;
                StartCoroutine(P2.Knockback(knocktime, thrust, this.transform));
                //rb.AddForce(difference, ForceMode2D.Impulse);
                Debug.Log("mental woi");
                //StartCoroutine(KnockCo(rbP2));
                //transform.position = new Vector2(transform.position.x + difference.x, transform.position.y + difference.y);
            }
            
        }
    }
    private IEnumerator KnockCo(Rigidbody2D rbPlayer)
    {
        if (rbPlayer != null)
        {
            yield return new WaitForSeconds(knocktime);
            player.berhentiMaju = false;
            //rbPlayer.velocity = Vector2.zero;
            //rbPlayer.isKinematic = true;
        }
    }
}
