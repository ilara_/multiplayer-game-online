﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player instance;

    public int id;
    public string username;
    public int animalId;
    //public CharacterController controller;
    public Rigidbody2D rb;
    public Transform shootOrigin;
    public float gravity = -9.81f;
    public float moveSpeed = 5f;
    public float jumpSpeed = 5f;
    public float throwForce = 600f;
    public float health;
    public float maxHealth = 100f;
    public int itemAmount = 0;
    public int maxItemAmount = 3;
    public float currentRotation;
    public float rotationSensitivity = 50.0f;
    public Quaternion rotation;

    public bool playerDie;
    public bool berhentiMaju = false;
    private bool[] inputs;
    private float yVelocity = 0;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        playerDie = false;
        gravity *= Time.fixedDeltaTime * Time.fixedDeltaTime;
        moveSpeed *= Time.fixedDeltaTime;
        jumpSpeed *= Time.fixedDeltaTime;
        rotationSensitivity *= Time.fixedDeltaTime;
    }

    public void Initialize(int _id, string _username,int _animalId)
    {
        id = _id;
        username = _username;
        animalId = _animalId;
        health = maxHealth;

        inputs = new bool[5];
    }

    void Rotation()
    {
        transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y, currentRotation));
        ServerSend.PlayerRotation(this);
        //rb.MoveRotation(currentRotation);
    }
    /// <summary>Processes player input and moves the player.</summary>
    public void FixedUpdate()
    {
        if (health <= 0f)
        {
            return;
        }

        Vector2 _inputDirection = Vector2.zero;
        if (inputs[0])
        {
            _inputDirection.y += 1;
        }
        if (inputs[1])
        {
            _inputDirection.y -= 1;
        }
        if (inputs[2])
        {
            currentRotation += rotationSensitivity;
            Rotation();
            //transform.Rotate(0, 0, 30, Space.Self);
            //transform.rotation = 
            //_inputDirection.x -= 1;
        }
        if (inputs[3])
        {
            currentRotation -= rotationSensitivity;
            Rotation();
            //_inputDirection.x += 1;
        }

        Move(_inputDirection);
    }

    /// <summary>Calculates the player's desired movement direction and moves him.</summary>
    /// <param name="_inputDirection"></param>
    private void Move(Vector2 _inputDirection)
    {
        //Vector2 _moveDirection = transform.right * _inputDirection.x + transform.up * _inputDirection.y;
        //transform.position += _moveDirection * moveSpeed; //gerak alternatif
        //rb.AddForce(rb.position + _moveDirection * moveSpeed);
        if (!berhentiMaju)
        {
            rb.AddForce(transform.up.normalized * moveSpeed, ForceMode2D.Impulse);
        }
        ServerSend.PlayerPosition(this);
    }

    /// <summary>Updates the player input with newly received input.</summary>
    /// <param name="_inputs">The new key inputs.</param>
    /// <param name="_rotation">The new rotation.</param>
    public void SetInput(bool[] _inputs, Quaternion _rotation)
    {
        inputs = _inputs;
        transform.rotation = _rotation;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("batas") && GameManager.gameStart)
        {
            Debug.Log("tabrak");
            if (playerDie == false)
            {
                //collision dari pemain disable
                //this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
                //Collision layer jadi dead
                gameObject.layer = 9;
                //ngirim bool pemain ini sudah mati
                playerDie = true;
                ServerSend.PlayerDie(playerDie, this);
                FindObjectOfType<GameManager>().DeadCount(id);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("batas") && GameManager.gameStart)
        {
            if (playerDie == false)
            {
                //collision dari pemain disable
                //this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
                //Collision layer jadi dead
                gameObject.layer = 9;
                //ngirim bool pemain ini sudah mati
                playerDie = true;
                ServerSend.PlayerDie(playerDie, this);
                FindObjectOfType<GameManager>().DeadCount(id);
            }
        }
    }

    public void Shoot(Vector3 _viewDirection)
    {
        if (health <= 0f)
        {
            return;
        }

        if (Physics.Raycast(shootOrigin.position, _viewDirection, out RaycastHit _hit, 25f))
        {
            if (_hit.collider.CompareTag("Player"))
            {
                _hit.collider.GetComponent<Player>().TakeDamage(50f);
            }
            else if (_hit.collider.CompareTag("Enemy"))
            {
                _hit.collider.GetComponent<Enemy>().TakeDamage(50f);
            }
        }
    }

    public void ThrowItem(Vector3 _viewDirection)
    {
        if (health <= 0f)
        {
            return;
        }

        if (itemAmount > 0)
        {
            itemAmount--;
            NetworkManager.instance.InstantiateProjectile(shootOrigin).Initialize(_viewDirection, throwForce, id);
        }
    }

    public void TakeDamage(float _damage)
    {
        if (health <= 0f)
        {
            return;
        }

        health -= _damage;
        if (health <= 0f)
        {
            health = 0f;
            //controller.enabled = false;
            transform.position = new Vector3(0f, 25f, 0f);
            ServerSend.PlayerPosition(this);
            StartCoroutine(Respawn());
        }

        ServerSend.PlayerHealth(this);
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(5f);

        health = maxHealth;
        //controller.enabled = true;
        ServerSend.PlayerRespawned(this);
    }

    public bool AttemptPickupItem()
    {
        if (itemAmount >= maxItemAmount)
        {
            return false;
        }

        itemAmount++;
        return true;
    }
    public IEnumerator Knockback(float knockbackDur, float knockbackPow, Transform obj)
    {
        berhentiMaju = true;
        float timer = 0;
        while (knockbackDur > timer)
        {
            timer += Time.deltaTime;
            Vector2 dir = (obj.transform.position - this.transform.position).normalized;
            rb.AddForce(-dir * knockbackPow);
        }
        berhentiMaju = false;
        yield return 0;
    }
}
