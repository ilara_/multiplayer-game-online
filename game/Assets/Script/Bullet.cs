﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    //public GameObject hitEffect;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
        //Destroy(effect, 5f);
        if (collision.gameObject.name == "Paladin" || collision.gameObject.tag == "Wall")
        {
            //gameObject.SetActive(false);
            //gameObject.GetComponent<SpriteRenderer>().color = Color.clear;
            StartCoroutine(Jeehad());
        }
    }
    IEnumerator Jeehad()
    {
        gameObject.transform.position = new Vector3(transform.position.x, transform.position.y, -10);
        Destroy(GetComponent<CircleCollider2D>());
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
}
