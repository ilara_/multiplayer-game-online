﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Countdown : MonoBehaviour
{
    public static float currentTime = 0f;
    public float startTime = 10f;
    public static bool startCountZone = false;
    [SerializeField] GameObject cdZone, zonaPanel;
    TextMeshProUGUI timeText;
    public void Start()
    {
        startCountZone = false;
        currentTime = startTime;
        timeText = cdZone.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
    }
    // Update is called once per frame
    void Update()
    {
        if (startCountZone == true)
        {
            currentTime -= 1 * Time.deltaTime;
            timeText.text = currentTime.ToString("0");
            if (!zonaPanel.activeInHierarchy)
                zonaPanel.SetActive(true);
            if (currentTime <= 7)
            {
                if (zonaPanel.activeInHierarchy)
                    zonaPanel.SetActive(false);
                if (currentTime <= 0)
                {
                    cdZone.SetActive(false);
                    zonaPanel.SetActive(false);
                    startCountZone = false;
                }
            }
        }
    }
    public void startZoneCount()
    {
        currentTime = startTime;
        startCountZone = true;
        cdZone.SetActive(true);
    }
}