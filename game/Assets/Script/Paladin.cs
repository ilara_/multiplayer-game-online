﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paladin : MonoBehaviour
{
    public float moveSpeed = 3f;
    Rigidbody2D rb;
    public Camera cam;
    [SerializeField] Timer timer;
    //[SerializeField] GameObject p1;
    Vector2 movement;
    Vector2 initPos;
    //Vector2 p1Pos;

    public float thrust;
    public float knocktime;
    // Start is called before the first frame update
    void Start()
    {
        initPos = gameObject.transform.position;
        rb = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKey(KeyCode.J))
        //{
        //    transform.position+=new Vector3(-1 * moveSpeed * Time.deltaTime, 0);
        //}
        //else if (Input.GetKey(KeyCode.L))
        //{
        //    transform.position += new Vector3(1 * moveSpeed * Time.deltaTime, 0);
        //}
        //else if (Input.GetKey(KeyCode.I))
        //{
        //    transform.position+=new Vector3(0, 1 * moveSpeed * Time.deltaTime);
        //}
        //else if (Input.GetKey(KeyCode.K))
        //{
        //    transform.position += new Vector3(0, -1 * moveSpeed * Time.deltaTime);
        //}
        //else
        //{
        //    transform.Translate(Vector3.zero);
        //}
        //p1Pos = p1.transform.position;
        //mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
    }
    private void FixedUpdate()
    {
        //Vector2 lookDir = p1Pos - rb.position;
        //float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        //rb.rotation = angle;
        //rb.AddForce(Vector2.right, ForceMode2D.Impulse);
        //rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
        //Vector2 lookDir = mousePos - rb.position;
        //float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        //rb.rotation = angle;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            gameObject.transform.position = initPos;
            rb.velocity = Vector2.zero;
            timer.CrownIn(1);
        }
    }
}
