﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatusTween : MonoBehaviour
{
    [SerializeField] GridLayoutGroup gridLayout;
    [SerializeField] float duration = .2f;
    void OnEnable()
    {
        LeanTween.value(gameObject, 70, 360, duration)
            .setOnUpdate((value) =>
            {
                gridLayout.cellSize = new Vector2(value, gridLayout.cellSize.y);
            });
        LeanTween.value(gameObject, 310, 20, duration)
            .setOnUpdate((value) =>
            {
                gridLayout.spacing = new Vector2(value, gridLayout.spacing.y);
            });
    }
}
