﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicturePodiumTween : MonoBehaviour
{
    [SerializeField] RectTransform rectTransform;
    [SerializeField] float startWidth, startHeight, endWidth, endHeight, duration;
    void OnEnable()
    {
        rectTransform.sizeDelta = new Vector2(startWidth, startHeight);
        rectTransform.LeanSize(new Vector2(endWidth, endHeight), duration).setEaseOutExpo().delay = 0.1f;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
