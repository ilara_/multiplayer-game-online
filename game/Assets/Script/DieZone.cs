﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class DieZone : MonoBehaviour
{
    public Vector3 zoneSize;
    public Vector3 zonePosition;

    public Transform ZonaMati;

    // Start is called before the first frame update
    void Start()
    {
        ZonaMati = transform.Find("zone");

        setZoneSize(new Vector3(-0.6499689f, -1f), new Vector3(6, 6));
    }

    public void setZoneSize(Vector3 position, Vector3 size)
    {
        zonePosition = position;
        zoneSize = size;

        transform.position = position;

        ZonaMati.localScale = size;
    }

    // Update is called once per frame
    void Update()
    {
      
    }

}
