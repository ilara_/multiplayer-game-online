﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeadTween : MonoBehaviour
{
    [SerializeField] RectTransform rectTransform;
    [SerializeField] bool selfDestroy;
    [SerializeField] float startWidth, startHeight, endWidth, endHeight, duration = .5f;
    void OnEnable()
    {
        rectTransform.sizeDelta = new Vector2(startWidth, startHeight);
        rectTransform.LeanSize(new Vector2(endWidth, endHeight), duration).setEaseOutExpo().delay = 0.1f;
        if (selfDestroy)
        {
            Invoke("DestroySelf", 5);
        }
    }
    void DestroySelf()
    {
        rectTransform.LeanSize(new Vector2(startWidth, startHeight), duration).setEaseInExpo().setOnComplete(OnComplete);
    }
    void OnComplete()
    {
        Destroy(gameObject);
    }
}
