﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Matchmaking : MonoBehaviour
{
    [SerializeField] Button buttonA, buttonB;
    [SerializeField] GameObject p1, p2, OkButton;
    [SerializeField] Sprite[] spriteP1, spriteP2;
    [SerializeField] Knockback bulletKnock;
    [SerializeField] TextMeshProUGUI judul, vs;
    [Header("Card 2")]
    [SerializeField] Image rolledImage;
    [SerializeField] TextMeshProUGUI textP2Speed, textP2Knok, textP2KnokTime, countDown;

    int randomP2 = 0;
    float mvSpeed = 0, thrst = 0, kncktime = 0;
    string animalName;
    private void Start()
    {
        buttonA.onClick.AddListener(delegate { Matching("A"); });
        buttonB.onClick.AddListener(delegate { Matching("B"); });
    }
    private void Matching(string agent)
    {
        buttonB.GetComponentInChildren<TextMeshProUGUI>().text = " ";
        textP2Speed.text = "Speed : -";
        textP2Knok.text = "Knock Force : -";
        textP2KnokTime.text = "Knock Duration : -";
        judul.text = "Matchmacking...";
        vs.text = "VS";

        if (agent == "A")
        {
            p1.GetComponent<SpriteRenderer>().sprite = spriteP1[0];
            p1.GetComponent<PlayerMovement>().moveSpeed = 2;
            bulletKnock.thrust = 1;
            bulletKnock.knocktime = 0.2f;
            randomP2 = Random.Range(0, 2);
            if (randomP2 == 0)
            {
                mvSpeed = 1.8f;
                thrst = 3;
                kncktime = 0.3f;
                animalName = "Lion";
            }
            else if (randomP2 == 1)
            {
                mvSpeed = 1.5f;
                thrst = 4;
                kncktime = 0.1f;
                animalName = "Cow";
            }
        }
        else if(agent == "B")
        {
            p1.GetComponent<SpriteRenderer>().sprite = spriteP1[1];
            p1.GetComponent<PlayerMovement>().moveSpeed = 4;
            bulletKnock.thrust = 6;
            bulletKnock.knocktime = 0.3f;
            randomP2 = Random.Range(2, 4);
            if (randomP2 == 2)
            {
                mvSpeed = 3.5f;
                thrst = 8;
                kncktime = 0.2f;
                animalName = "Dog";
            }
            else if (randomP2 == 3)
            {
                mvSpeed = 4.3f;
                thrst = 7;
                kncktime = 0.4f;
                animalName = "Bull";
            }
        }
        StartCoroutine(RollTheSprite(randomP2));
        p2.GetComponent<SpriteRenderer>().sprite = spriteP2[randomP2];
        p2.GetComponent<Paladin>().moveSpeed = mvSpeed;
        Knockback knockbackp2 = p2.GetComponent<Knockback>();
        knockbackp2.thrust = thrst;
        knockbackp2.knocktime = kncktime;
    }
    public IEnumerator RollTheSprite(int option)
    {
        // Variable to contain random dice side number.
        // It needs to be assigned. Let it be 0 initially
        int randomDiceSide = 0;

        // Final side or value that dice reads in the end of coroutine
        int finalSide = 0;

        // Loop to switch dice sides ramdomly
        // before final side appears. 20 itterations here.
        for (int i = 0; i <= 40; i++)
        {
            // Pick up random value from 0 to 5 (All inclusive)
            randomDiceSide = Random.Range(0, spriteP2.Length);

            // Set sprite to upper face of dice from array according to random value
            rolledImage.sprite = spriteP2[randomDiceSide];

            // Pause before next itteration
            yield return new WaitForSeconds(0.1f);
        }
        //tampilkan kartu match enemy
        rolledImage.sprite = spriteP2[option];
        judul.text = "Match Found!";
        textP2Speed.text = "Speed : " + mvSpeed;
        textP2Knok.text = "Knock Force : " + thrst;
        textP2KnokTime.text = "Knock Time : " + kncktime;
        buttonB.GetComponentInChildren<TextMeshProUGUI>().text = animalName;
        OkButton.SetActive(true);
        // Assigning final side so you can use this value later in your game
        // for player movement for example
        finalSide = randomDiceSide + 1;

        // Show final dice value in Console
        Debug.Log(finalSide);
    }
}
