﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knockback : MonoBehaviour
{
    public float thrust;
    public float knocktime;
    // Start is called before the first frame update
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Rigidbody2D rbP2 = collision.gameObject.GetComponent<Rigidbody2D>();
            
            if (rbP2 != null)
            {
                rbP2.isKinematic = false;
                Vector2 difference = rbP2.transform.position - transform.position;
                difference = difference.normalized * thrust;
                rbP2.AddForce(difference, ForceMode2D.Impulse);
                StartCoroutine(KnockCo(rbP2));
            }
            //transform.position = new Vector2(transform.position.x + difference.x, transform.position.y + difference.y);
        }
    }
    private IEnumerator KnockCo(Rigidbody2D rbPlayer)
    {
        if(rbPlayer != null)
        {
            yield return new WaitForSeconds(knocktime);
            rbPlayer.velocity = Vector2.zero;
            //rbPlayer.isKinematic = true;
        }
    }
}
