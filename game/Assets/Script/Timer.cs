﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Timer : MonoBehaviour
{
    float currentTime = 0f;
    int p1, p2 = 0;
    [SerializeField] float startTime = 90f;
    [SerializeField] TextMeshProUGUI textP1Crown, textP2Crown, reminder;
    [SerializeField] GameObject gameOver;
    private TextMeshProUGUI CountDownText;
    void Start()
    {
        CountDownText = gameObject.GetComponent<TextMeshProUGUI>();
        currentTime = startTime;
    }
    // Update is called once per frame
    void Update()
    {
        if (currentTime > 0)
        {
            currentTime -= 1 * Time.deltaTime;
            CountDownText.text = currentTime.ToString("0");
        }
        else
        {
            gameOver.GetComponentInChildren<TextMeshProUGUI>().text = "Game Over\nPlayer " + ((p1>p2) ? 1 : 2) + " Win!";
            gameOver.SetActive(true);
        }
        if (currentTime <= 45) 
        {
            CountDownText.color = Color.magenta;
            reminder.text = "45 Sec Passed\nKill = 2 Crown";
        }
        if (currentTime <= 40)
            reminder.text = " ";
        if (currentTime <= 15) 
        {
            CountDownText.color = Color.red;
            reminder.text = "Frenzy Time!!\nKill = 3 Crown!";
        }
        if (currentTime <= 10)
            reminder.text = " ";
    }
    public void CrownIn(int a)
    {
        if (a==1)
        {
            p1 = p1 + CrownByTime();
            textP1Crown.text = "Crown : " + p1.ToString();
        }
        else if (a == 2)
        {
            p2 = p2 + CrownByTime();
            textP2Crown.text = "Crown : " + p2.ToString();
        }
    }
    private int CrownByTime()
    {
        int ret;
        if (currentTime > 45)
            ret = 1;
        else if (currentTime > 15)
            ret = 2;
        else
            ret = 3;
        return ret;
    }
}