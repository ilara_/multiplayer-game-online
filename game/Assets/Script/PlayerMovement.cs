﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 3f;
    public Rigidbody2D rb;
    public Camera cam;
    //[SerializeField] Timer timer;
    Vector2 mousePos;
    Vector2 initPos;
    // Start is called before the first frame update
    void Start()
    {
        initPos = gameObject.transform.position;
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
    }
    // Update is called once per frame
    void Update()
    {
        float geser = Input.GetAxisRaw("Horizontal") *moveSpeed;
        float naik = Input.GetAxisRaw("Vertical") *moveSpeed;
        geser *= Time.deltaTime;
        naik *= Time.deltaTime;
        transform.position+=new Vector3(geser, 0);
        transform.position+=new Vector3(0,naik);

        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
    }
    private void FixedUpdate()
    {
        Vector2 lookDir = mousePos - rb.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        rb.rotation = angle;
    }
    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    //if (collision.gameObject.name == "Paladin")
    //    //{
    //    //    Vector2 difference = transform.position - collision.transform.position;
    //    //    transform.position = new Vector2(transform.position.x + difference.x + 0.5f, transform.position.y + difference.y + 0.5f);
    //    //}
    //    if (collision.gameObject.tag == "Wall")
    //    {
    //        gameObject.transform.position = initPos;
    //        timer.CrownIn(2);
    //    }
    //}
}
