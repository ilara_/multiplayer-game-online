﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepRotation : MonoBehaviour
{
    [SerializeField] GameObject parent;
    Vector3 initPos;
    void Start()
    {
        initPos = gameObject.transform.localPosition;
    }
    void LateUpdate()
    {
        transform.position = parent.transform.localPosition + initPos;
        transform.rotation = Quaternion.identity;
    }
}
