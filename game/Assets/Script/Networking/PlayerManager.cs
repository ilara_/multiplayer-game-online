﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerManager : MonoBehaviour
{
    public int id;
    public string username;
    public int animalId;
    public float health;
    public float maxHealth = 100f;
    public int itemCount = 0;
    public Rigidbody2D rb;
    [SerializeField] GameObject bgEmoji;
    [SerializeField] Sprite[] emojis;
    //public static int playerDieCounter;
    SpriteRenderer playerOpp;
    [SerializeField] TextMeshProUGUI unameIngame;
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Sprite[] animalSprites;
    [SerializeField] GameObject canvasNameBar;
   
    public void Initialize(int _id, string _username, int _animalId)
    {
        id = _id;
        username = _username;
        animalId = _animalId;
        health = maxHealth;
        unameIngame.text = username;
        spriteRenderer.sprite = animalSprites[animalId];
        playerOpp = GetComponent<SpriteRenderer>();
    }

    public void SetHealth(float _health)
    {
        health = _health;

        if (health <= 0f)
        {
            Die();
        }
    }

    public void Die()
    {

        GameManager.playerDieCounter += 1;
        //Debug.Log(GameManager.playerDieCounter);
        playerOpp.color = new Color(1f, 1f, 1f, .5f);
        bgEmoji.GetComponent<Image>().color = new Color32(238, 78, 78, 255);
        bgEmoji.transform.GetChild(0).GetComponent<Image>().sprite = emojis[0];
        return;
        //model.enabled = false;
    }

    public void Respawn()
    {
        //model.enabled = true;
        SetHealth(maxHealth);
    }
    private void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            canvasNameBar.SetActive(UIManager.instance.nameEnabled);
        }
    }
}
