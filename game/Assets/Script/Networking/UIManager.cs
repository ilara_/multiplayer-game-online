﻿using Proyecto26;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public List<int> playerRankSort;
    public GameObject logo;
    public GameObject participant;

    [Header("Login")]
    public InputField usernameField;
    public InputField passwordField;
    public InputField regUsernameField;
    public InputField regPasswordField;
    public InputField regConfPasswordField;
    [SerializeField] TextMeshProUGUI errorMsgLogin, errorMsgReg, userWelcome;
    bool regInputValid;
    [SerializeField] GameObject loading, registerButton, startMenu, welcomePanel, lockMaung;
    [SerializeField] RectTransform transitionPanel;

    public TextMeshProUGUI peserta;
    public static int totalPeserta;
    UserData userData = new UserData(string.Empty, string.Empty, false);
    [SerializeField] GameObject guide, playerDeadUI, deadContainer, playerStatus, statusContainer, tabUIArea, gameOverPanel, cdZone, zoneNotif;
    [SerializeField] Sprite[] emojiSprites, bgSprites;
    public bool nameEnabled = true;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }

    }
    public void SetAnimalId(int id)
    {
        Client.instance.animalId = id;
    }
    /// <summary>Attempts to connect to the server.</summary>
    public void ConnectToServer()
    {
        Transitioning(new GameObject[] { startMenu, welcomePanel }, 2);
        Client.instance.uname = usernameField.text;
        Client.instance.pass = passwordField.text;
        usernameField.interactable = false;
        Client.instance.ConnectToServer();
    }

    public void CheckRegisterInput()
    {
        if (regUsernameField.text.Length >= 4 && regUsernameField.text.Length <= 14)
        {
            errorMsgReg.text = "";
            if (regPasswordField.text.Length >= 4)
            {

                errorMsgReg.text = "";
                if (regPasswordField.text == regConfPasswordField.text)
                {
                    errorMsgReg.text = "";
                    regInputValid = true;
                }
                else if(regConfPasswordField.text != "")
                {
                    errorMsgReg.text = "Password does not match!";
                    errorMsgReg.color = Color.red;
                    regInputValid = false;
                }
            }
            else if(regPasswordField.text != "")
            {
                errorMsgReg.text = "Password must be at least 4 characters!";
                errorMsgReg.color = Color.red;
                regInputValid = false;
            }
        }
        else
        {
            errorMsgReg.text = "Username must between 4 and 14 characters!";
            errorMsgReg.color = Color.red;
            regInputValid = false;
        }
    }

    public void Register()
    {
        if (regInputValid)
        {
            loading.SetActive(true);
            RestClient.Get<UserData>("https://praktikum-jaringan-komputer-default-rtdb.asia-southeast1.firebasedatabase.app/" + regUsernameField.text + ".json").Then(response =>
            {
                errorMsgReg.text = "Username already exist!";
            }, response =>
            {
                Debug.Log("registering");
                userData = new UserData(regUsernameField.text, regPasswordField.text, false);
                RestClient.Put("https://praktikum-jaringan-komputer-default-rtdb.asia-southeast1.firebasedatabase.app/" + regUsernameField.text + ".json", userData).Done(responseSucces => {
                    errorMsgReg.text = "Register done!";
                    errorMsgReg.color = Color.green;
                    Debug.Log(responseSucces);
                    registerButton.SetActive(false);
                }, responseFail =>
                {
                    errorMsgReg.text = "Register fail, try again later!";
                    errorMsgReg.color = Color.green;
                    Debug.Log(responseFail);
                });
            }).Finally(()=> loading.SetActive(false)).Done();
        }
    }
    public void LoginRequest()
    {
        loading.SetActive(true);
        RestClient.Get<UserData>("https://praktikum-jaringan-komputer-default-rtdb.asia-southeast1.firebasedatabase.app/" + usernameField.text + ".json").Then((System.Action<UserData>)(response =>
        {
            if (response.username == usernameField.text && response.password == passwordField.text)
            {
                Debug.Log("login success");
                userWelcome.text = usernameField.text;
                if(response.unlock)
                    lockMaung.SetActive(false);
                Transitioning(startMenu, welcomePanel, 1f);
            }
            else
            {
                errorMsgLogin.text = "Login failed!";
            }
        }), response =>
        {
            errorMsgLogin.text = "Login failed!";
        }).Finally(()=> loading.SetActive(false));
    }
    void Transitioning(GameObject hide, GameObject active, float duration)
    {
        loading.SetActive(true);
        transitionPanel.gameObject.SetActive(true);
        LeanTween.alpha(transitionPanel, 1f, duration).setEaseInOutExpo().setOnComplete(() =>
        {
            hide.SetActive(false);
            active.SetActive(true);
            LeanTween.alpha(transitionPanel, 0f, duration).setEaseInOutExpo().setOnComplete(() =>
            {
                loading.SetActive(false);
                transitionPanel.gameObject.SetActive(false);
            });
        });
    }
    void Transitioning(GameObject[] hide, float duration)
    {
        loading.SetActive(true);
        transitionPanel.gameObject.SetActive(true);
        LeanTween.alpha(transitionPanel, 1f, duration).setEaseInOutExpo().setOnComplete(() =>
        {
            logo.SetActive(true);
            participant.SetActive(true);
            foreach (GameObject g in hide)
            {
                g.SetActive(false);
            }
            LeanTween.alpha(transitionPanel, 0f, duration).setEaseInOutExpo().setOnComplete(() =>
            {
                loading.SetActive(false);
                transitionPanel.gameObject.SetActive(false);
                if(guide != null)
                {
                    guide.SetActive(true);
                }
            });
        });
    }
    private void RetrieveFromDatabase()
    {
        RestClient.Get<UserData>("https://praktikum-jaringan-komputer-default-rtdb.asia-southeast1.firebasedatabase.app/" + usernameField.text + ".json").Then(response =>
        {
            userData = response;
        });
    }
    private void Update()
    {
        peserta.text = GameManager.playerAlive + " / " + GameManager.totalPlayer;
        //Debug.Log(totalPeserta);
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            tabUIArea.SetActive(true);
        }
        else if (Input.GetKeyUp(KeyCode.Tab))
        {
            tabUIArea.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            nameEnabled = !nameEnabled;
        }
    }
    public void ShowDeadPlayer(int _id, string _uname)
    {
        GameObject iplayerDeadUI = Instantiate(playerDeadUI);
        iplayerDeadUI.GetComponentInChildren<TextMeshProUGUI>().text = _uname;
        iplayerDeadUI.transform.SetParent(deadContainer.transform, false);
        ChangePlayerStatus(_id, 0, -2); //0 untuk dead sprite dan bg merah
    }
    public void AddPlayerList(int _id, string _uname)
    {
        GameObject iplayerDeadUI = Instantiate(playerStatus);
        iplayerDeadUI.name = _id.ToString();
        iplayerDeadUI.GetComponentInChildren<TextMeshProUGUI>().text = _uname;
        iplayerDeadUI.transform.SetParent(statusContainer.transform, false);
    }
    public void ChangePlayerStatus(int id, int emojiCode, int bgCode)
    {
        try
        {
            Transform bgEmoji = statusContainer.transform.Find(id.ToString()).GetChild(1);
            if (bgCode == -2)
            {
                bgEmoji.GetComponent<Image>().color = new Color32(238, 78, 78, 255); //karena ada bug versi pake color32, color supposed to work fine
                //Debug.Log("ganti color");
            }
            else if (bgCode == -1)
            {
                bgEmoji.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            }
            else
            {
                bgEmoji.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                bgEmoji.GetComponent<Image>().sprite = bgSprites[bgCode];
            }
            bgEmoji.GetChild(0).GetComponent<Image>().sprite = emojiSprites[emojiCode];
        }
        catch
        {
            Debug.Log("cannot find player status");
        }
    }
    public void GameOver(List<int> _playerRankSort)
    {
        logo.SetActive(false);
        participant.SetActive(false);
        cdZone.SetActive(false);
        zoneNotif.SetActive(false);
        playerRankSort = _playerRankSort;
        gameOverPanel.SetActive(true);
    }
}
