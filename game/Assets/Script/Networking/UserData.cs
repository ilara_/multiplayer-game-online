﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UserData
{
    public string username;
    public string password;
    public bool unlock;
    public UserData(string user, string pass, bool _unlock)
    {
        username = user;
        password = pass;
        unlock = _unlock;
    }
}
