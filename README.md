# Desain Multiplayer Game Online / Boom Boom Bounce

Ilham Agung Riyadi / 4210181023      

Frederiko Adrian R.B / 4210181024

![](https://img.itch.zone/aW1nLzYzOTU2ODQucG5n/347x500/Xxd1f3.png) 

### Description:

Boom Boom Bounce is a game where players assume the role of a circular character in a match arena. Players consist of more than three players. Each player can move his character in real-time. Player characters will be placed in a game area together with other players to compete with each other. The player can explore various strategies such as acting passively or actively to defend his position.

### General Information

Platform    : Windows/ Desktop

Genre       : Multiplayer, Casual, Strategy, Battle Royale

---

### How the Gameplay looks like?


![Alt Text](gif_gameplay.gif)

---
### Game Flow

![](flow.png) 

Player registers to create an account and continues to login using the account that has been registered. If players want to play right away, they can directly play "play as a guest". if the login is successful the player can enter the game and wait for other players to enter.
The protocols used for this final game project are TCP and UDP. TCP is used to open the path of communication / information exchange for the next data exchange which then uses UDP to send data. Data sent using packets and protocols for delivery varies according to the sensitivity level. If data is sent continuously and greatly affects the course of the game, for example the movement of players, then the data packet is sent using UDP, for dead player data that affects the participant's display, it is biased using TCP.

---

# Documentation : Boom Boom Bounce


| Game Link | GDD Link |
| ------ | ------ |
|[Itch.io](https://adriian.itch.io/boom-boom-bounce)| [Game Design Document](https://docs.google.com/document/d/1uD4IUk_nac0poJq0-FCWCGvMN8eTdy7v2Kjez_GtfMw/edit)|


## User Autentication
### Register
Checks if the username already exists, if not then performs a PUT request to the database.
```
public void Register()
{
    if (regInputValid)
    {
        //Get request to database if whether username is exist
        RestClient.Get<UserData>("https://praktikum-jaringan-komputer-default-rtdb.asia-southeast1.firebasedatabase.app/" + regUsernameField.text + ".json").Then(response =>
        {
            errorMsgReg.text = "Username already exist!";
        }, response =>
        {
            Debug.Log("registering");
            userData = new UserData(regUsernameField.text, regPasswordField.text, false);
            RestClient.Put("https://praktikum-jaringan-komputer-default-rtdb.asia-southeast1.firebasedatabase.app/" +regUsernameField.text + ".json", userData).Done(responseSucces => {
            errorMsgReg.text = "Register done!";
        }
    }
}
```
The above process is a process where checking data in the form of a username and password which will be sent and checking between the data that has been inputted and the data in the database is the same. If there are the same, the server sends a message to the client in the form of "Username already exists!" and if none are the same, the server sends a message to the client in the form of "Register done!" as a notification to the client.
### Login

Equalize data between databases with GET requests and inputs
```
public void LoginRequest()
{
    //Match database with input
    RestClient.Get<UserData>("https://praktikum-jaringan-komputer-default-rtdb.asia-southeast1.firebasedatabase.app/" + usernameField.text + ".json").Then((System.Action<UserData>)(response =>
    {
        if (response.username == usernameField.text && response.password == passwordField.text)
        {
            Debug.Log("login success");
        }
        else
        {
            errorMsgLogin.text = "Login failed!";
        }
    }), response =>
    {
        errorMsgLogin.text = "Login failed!";
    })
}
```
The above process is a process where the player enters data in the form of a username and password which will be sent and checks between the data that has been inputted and the data in the database whether it matches. If it does not match the player cannot enter the game. the server sends a login failed message to the client.

## Player Spawn
Get information from newly connected players in the form of username, Id and selected character. And continued on the local function to generate the spawn position and rotation of new players.
```
public static void WelcomeReceived(int _fromClient, Packet _packet)
{
    int _clientIdCheck = _packet.ReadInt();
    string _username = _packet.ReadString();
    int _animalId = _packet.ReadInt();

    Debug.Log("username = " + _username);
    Debug.Log($"{Server.clients[_fromClient].tcp.socket.Client.RemoteEndPoint} connected successfully and is now player {_fromClient}.");

    if (_fromClient != _clientIdCheck)
    {
        Debug.Log($"Player \"{_username}\" (ID: {_fromClient}) has assumed the wrong client ID ({_clientIdCheck})!");
    }
    Server.clients[_fromClient].SendIntoGame(_username, _animalId);
}
```
Sends information to new players, all previously connected players and to already connected players about new players.
```
public void SendIntoGame(string _playerName, int _animalId)
{
    player = NetworkManager.instance.InstantiatePlayer();
    player.Initialize(id, _playerName, _animalId);

    // Send all players to the new player
    foreach (Client _client in Server.clients.Values)
    {
        if (_client.player != null)
        {
            if (_client.id != id)
            {
                ServerSend.SpawnPlayer(id, _client.player);
            }
        }
    }

    // Send the new player to all players (including himself)
    foreach (Client _client in Server.clients.Values)
    {
        if (_client.player != null)
        {
            ServerSend.SpawnPlayer(_client.id, player);
        }
    }
}
```
Sending information in the form of id, username, position, initial rotation, and the character chosen by the player to be instantiated to each client.
```
public static void SpawnPlayer(int _toClient, Player _player)
{
    using (Packet _packet = new Packet((int)ServerPackets.spawnPlayer))
    {
        _packet.Write(_player.id);
        _packet.Write(_player.username); 
        _packet.Write(_player.transform.position);
        _packet.Write(_player.transform.rotation);
        _packet.Write(_player.animalId);

        SendTCPData(_toClient, _packet);
    }
}
```
>The spawn player information is sent via the TCP protocol because the integrity of the data is considered important.

The client side reads the packet and gets information about the players that need to be spawned. The client instantiates based on that information.
The related information includes id, username, position, rotation, animalId(character).
```
public static void SpawnPlayer(Packet _packet)
{
    int _id = _packet.ReadInt();
    string _username = _packet.ReadString();
    Vector3 _position = _packet.ReadVector3();
    Quaternion _rotation = _packet.ReadQuaternion();
    int animalId = _packet.ReadInt();

    GameManager.instance.SpawnPlayer(_id, _username, _position, _rotation, animalId);
}
```
## Player Control

Sends input as a boolean array


    private void SendInputToServer()
    {
        bool[] _inputs = new bool[]
        {
            Input.GetKey(KeyCode.W),
            Input.GetKey(KeyCode.S),
            Input.GetKey(KeyCode.A),
            Input.GetKey(KeyCode.D),
            Input.GetKey(KeyCode.Space)
        };
        ClientSend.PlayerMovement(_inputs);
    };
    

This is a function that captures input from the user which will be sent via a function in the client send.
    
    private void FixedUpdate()
    {
        SendInputToServer();
    }

The function continues as long as the application is running.

    public static void PlayerMovement(bool[] _inputs)
    {
        using (Packet _packet = new Packet((int)ClientPackets.playerMovement))
        {
            _packet.Write(_inputs.Length);
            foreach (bool _input in _inputs)
            {
                _packet.Write(_input);
            }
            _packet.Write(GameManager.players[Client.instance.myId].transform.rotation);

            SendUDPData(_packet);
        }
    }

A function that converts the previous user input into a new packet and sends it by UDP to the server.

    public enum ClientPackets
    {
        welcomeReceived = 1,
        playerMovement,
    }
    
Provide identity to packages that have been previously created on the client.

    public enum ClientPackets
    {
        welcomeReceived = 1,
        playerMovement,
    }
    
Identify the identity of the packet that has been received previously from the client, the packet enum declaration must be the same between the client and server.

     private static void InitializeServerData()
    {
        for (int i = 1; i <= MaxPlayers; i++)
        {
            clients.Add(i, new Client(i));
        }

        packetHandlers = new Dictionary<int, PacketHandler>()
        {
            { (int)ClientPackets.welcomeReceived, ServerHandle.WelcomeReceived },
            { (int)ClientPackets.playerMovement, ServerHandle.PlayerMovement }
        };
        Debug.Log("Initialized packets.");
    }

Initializes the enum passed to the local packet dissecting function on the server.

     public static void PlayerMovement(int _fromClient, Packet _packet)
    {
        bool[] _inputs = new bool[_packet.ReadInt()];
        for (int i = 0; i < _inputs.Length; i++)
        {
            _inputs[i] = _packet.ReadBool();
        }
        Quaternion _rotation = _packet.ReadQuaternion();

        Server.clients[_fromClient].player.SetInput(_inputs, _rotation);
    }
    
The above is a package dissection function on the server.

    public void SetInput(bool[] _inputs, Quaternion _rotation)
    {
        inputs = _inputs;
        transform.rotation = _rotation;
    }
    
Initializes data from packet dissection to local variables on the server.

    Vector2 _inputDirection = Vector2.zero;
        if (inputs[0])
        {
            _inputDirection.y += 1;
        }
        if (inputs[1])
        {
            _inputDirection.y -= 1;
        }
        if (inputs[2])
        {
            currentRotation += rotationSensitivity;
            Rotation();
        }
        if (inputs[3])
        {
            currentRotation -= rotationSensitivity;
            Rotation();
        }
        Move(_inputDirection);
        
Inputs and rotations that have been initialized to local variables, are entered into functions whose task is to pass on to local calculations the x, y positions and also the rotations performed on the server.

    void Rotation()
    {
        transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y, currentRotation));
        ServerSend.PlayerRotation(this);
    }
    
Player rotation calculation function on the server.

    private void Move(Vector2 _inputDirection)
    {
        if (!berhentiMaju)
        {
            rb.AddForce(transform.up.normalized * moveSpeed, ForceMode2D.Impulse);
        }
        ServerSend.PlayerPosition(this);
    }
    
The function of calculating the player's position on the server.

    public static void PlayerRotation(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerRotation))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.transform.rotation);

            SendUDPDataToAll(_packet);
        }
    }
    
The function in charge of packaging the value of the rotation variable is then continued by sending via UDP on the server.

    public static void PlayerPosition(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerPosition))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.transform.position);

            SendUDPDataToAll(_packet);
        }
    }
    
The function in charge of packaging the value of the position variable is then continued by sending via UDP on the server.

    public enum ServerPackets
    {
        welcome = 1,
        spawnPlayer,
        playerPosition,
        playerRotation,
        playerDisconnected,
        cdStart,
        playerDie,
        gameOver,
        zoneStart
    }
    
Gives identity to pre-generated packages on the server.

    public enum ServerPackets
    {
        welcome = 1,
        spawnPlayer,
        playerPosition,
        playerRotation,
        playerDisconnected,
        cdStart,
        playerDie,
        gameOver,
        zoneStart
    }
    
Checks the identity of packets sent from the server on the client. The packet enum declaration must be the same between the client and the server.

    
    private void InitializeClientData()
    {
        packetHandlers = new Dictionary<int, PacketHandler>()
        {
            { (int)ServerPackets.welcome, ClientHandle.Welcome },
            //{ (int)ServerPackets.loginReq, ClientHandle.Login },
            { (int)ServerPackets.spawnPlayer, ClientHandle.SpawnPlayer },
            { (int)ServerPackets.playerPosition, ClientHandle.PlayerPosition },
            { (int)ServerPackets.playerRotation, ClientHandle.PlayerRotation },
            { (int)ServerPackets.playerDisconnected, ClientHandle.PlayerDisconnected },
            { (int)ServerPackets.cdStart, ClientHandle.CountdownStart },
            { (int)ServerPackets.zoneStart, ClientHandle.ZoneStart },
            { (int)ServerPackets.playerDie, ClientHandle.PlayerDie },
            { (int)ServerPackets.gameOver, ClientHandle.GameOver },
        };
        Debug.Log("Initialized packets.");
    }

Initializes the enum passed to the local packet dissection function on the client.

    public static void PlayerPosition(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            _player.rb.MovePosition(_position);
        }
    }
    
    public static void PlayerRotation(Packet _packet)
    {
        
        int _id = _packet.ReadInt();
        Quaternion _rotation = _packet.ReadQuaternion();
       
        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            
            _player.transform.rotation = _rotation;
        }
    }

The package surgery function on the client and immediately changes the existing value in the local variable with the new value from the package surgery result.

## Dead Zone

Is a mechanism that makes the player's movement progressively smaller and smaller.

    if (stopwatchActive == true)
    {
        currentTime = currentTime + Time.deltaTime;
    }
    TimeSpan time = TimeSpan.FromSeconds(currentTime);
    if (time.TotalSeconds >= 30 && time.TotalSeconds < 31)
    {
        ServerSend.ZoneStart((int)currentTime, zonePosition, zoneSize);
    }
    if (time.TotalSeconds >= 40 && time.TotalSeconds < 70) //zona 1
    {
        scaleDownZone1();
    }
    if (time.TotalSeconds >= 110 && time.TotalSeconds < 111)
    {
        ServerSend.ZoneStart((int)currentTime, zonePosition, zoneSize);
    }
    if (time.TotalSeconds >= 120) //zona 1
    {
        scaleDownZone2();
    }

The function above is an update function that is on the server to perform calculations continuously from local functions which will later be sent to the client.
    public void scaleDownZone1()
    {
       
        targetZoneSize = new Vector3(2f, 2f);
        targetZonePosition = new Vector3(-0.5f, 1.1f);

        Vector3 sizeChange = (targetZoneSize - zoneSize).normalized;
        Vector3 newZoneSize = zoneSize + sizeChange * Time.deltaTime * zoneSpeed;

        Vector3 zoneMoveDir = (targetZonePosition - zonePosition).normalized;
        Vector3 newZonePosition = zonePosition + zoneMoveDir * Time.deltaTime * zoneSpeed;
        

        setZoneSize(newZonePosition, newZoneSize);
        ServerSend.ZoneStart((int)currentTime, zonePosition, zoneSize);
    }
    
    private void setZoneSize(Vector3 position,Vector3 size)
    {
        zonePosition = position;
        zoneSize = size;

        transform.position = position;

        ZonaMati.localScale = size;
    }

The above is a function of calculating the position and size of the zone whose calculations are carried out on the server and the results will be sent via the send function to the client that was in the previous update function.

    public void StartStopwatch()
    {
        stopwatchActive = true;
    }

    public void StopStopwatch()
    {
        stopwatchActive = false;
    }

    public void ResetStopwatch()
    {
        stopwatchActive = false;
        currentTime = 0;
        setZoneSize(new Vector3(-0.6499689f, -1f), new Vector3(6, 6));
    }
    
The above is a server stopwatch function that functions to trigger the function of changing the position and size of the zone which is also sent to the client which functions to trigger the emergence of a countdown to the client before zone reduction is carried out.

    public static void ZoneStart(int time, Vector3 position, Vector3 size)
    {
        using (Packet _packet = new Packet((int)ServerPackets.zoneStart))
        {
            _packet.Write(time);
            _packet.Write(position);
            _packet.Write(size);

            SendUDPDataToAll(_packet);
        }
    }

The function above is useful for packaging the results of the calculation of local variables, namely the zone size, zone position and stopwatch and then sending it to the client via UDP.

    public enum ServerPackets
    {
        welcome = 1,
        spawnPlayer,
        playerPosition,
        playerRotation,
        playerDisconnected,
        cdStart,
        playerDie,
        gameOver,
        zoneStart
    }
    
Gives identity to pre-generated packages on the server.

    public enum ServerPackets
    {
        welcome = 1,
        spawnPlayer,
        playerPosition,
        playerRotation,
        playerDisconnected,
        cdStart,
        playerDie,
        gameOver,
        zoneStart
    }
    
Checks the identity of packets sent from the server on the client. The packet enum declaration must be the same between the client and the server.
    
    private void InitializeClientData()
    {
        packetHandlers = new Dictionary<int, PacketHandler>()
        {
            { (int)ServerPackets.welcome, ClientHandle.Welcome },
            //{ (int)ServerPackets.loginReq, ClientHandle.Login },
            { (int)ServerPackets.spawnPlayer, ClientHandle.SpawnPlayer },
            { (int)ServerPackets.playerPosition, ClientHandle.PlayerPosition },
            { (int)ServerPackets.playerRotation, ClientHandle.PlayerRotation },
            { (int)ServerPackets.playerDisconnected, ClientHandle.PlayerDisconnected },
            { (int)ServerPackets.cdStart, ClientHandle.CountdownStart },
            { (int)ServerPackets.zoneStart, ClientHandle.ZoneStart },
            { (int)ServerPackets.playerDie, ClientHandle.PlayerDie },
            { (int)ServerPackets.gameOver, ClientHandle.GameOver },
        };
        Debug.Log("Initialized packets.");
    }

Initializes the enum passed to the local packet dissection function on the client.

    public static void ZoneStart(Packet _packet)
    {
        int time = _packet.ReadInt();
        Vector3 position = _packet.ReadVector3();
        Vector3 size = _packet.ReadVector3();

        GameManager.instance.StartZone(time, position, size);
    }
    
The package surgery function on the client and immediately changes the existing value in the local variable with the new value from the package surgery result.

    public void StartZone(int time, Vector3 position, Vector3 size)
    {
        zona.GetComponent<DieZone>().setZoneSize(position, size);
        if(time >= 30 && time < 40 )
        {
            FindObjectOfType<Countdown>().startZoneCount();
        }


        if (time >= 110 && time < 120)
        {
            FindObjectOfType<Countdown>().startZoneCount();
        }

    }
    
After the data change occurs, for some conditions it will perform local functions including the appearance of a warning notification before the zone shrinks with a stopwatch from the server as the trigger.

## Game Over
Every player who collides with a barrier (dead) between the beach and deadzone will call this function by providing its id parameter to calculate the number of losing players. The id is entered into the list so that it provides information on the order in which the player loses. If there is 1 remaining live player, it will add the id to the list and send the list as the result of the game and a sign of the end of the game.
```
public void DeadCount(int id)
{
    playerAlive--;
    playerRankSort.Add(id);
    Debug.Log("current player alive : " + playerAlive);
    if (playerAlive <= 1)
    {
        playerRankSort.Add(getMissingNo(playerRankSort, playerRankSort.Count)); //getting last man standing id
        ServerSend.GameOver(playerRankSort);
        ResetGame();
    }
}
```
Decomposes the list into ints to be inserted into the packet stream sequentially and sent.
```
public static void GameOver(List<int> _playerDeadSort)
{
    using (Packet _packet = new Packet((int)ServerPackets.gameOver))
    {
        _packet.Write(_playerDeadSort.Count);
        _playerDeadSort.ForEach(_packet.Write);
        SendTCPDataToAll(_packet);
    }
}
```
>Information on game results / player rankings is sent via the TCP protocol because the integrity of the data is considered important.

On the client side, packets are received in the order of the losing player id. The sequence is entered into the player rank list to find relevant player information and is displayed sequentially when the game is over.
```
public static void GameOver(Packet _packet)
{
    int length = _packet.ReadInt();
    List<int> playerRankSort = new List<int>(length);
    for (int i = 1; i <= length; i++)
    {
        playerRankSort.Add(_packet.ReadInt());
    }
    UIManager.instance.GameOver(playerRankSort);
}
```
> The above process is carried out more or less like a broadcasting system that displays data simultaneously to all connected players/users.
---

